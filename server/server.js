"use strict";

var http = require('http');
var express = require('express');
var app = express();
var path = require("path");
var mongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost/blacken';


//set public directory
app.use(express.static(path.join(__dirname, 'public')));


//stream index.html file
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname,'views/index.html'));
});

//get json-Data from the database
app.get('/data.json',function(req,res)
{
	
	mongoClient.connect(url, function(err, db) {

		db.collection('position').distinct('configName',function(err,config)
		{
			let minimalPercentBlacken = req.query.sliderrangeBlacken*0.01;
			let minimalPercentGrass = 1-minimalPercentBlacken
			
			let orQuery = [];
			let configName = '';
			if(typeof req.query.config !== typeof undefined && req.query.config !== '')
				configName = req.query.config;
			else
				configName = config[0];
			
			console.log(configName);
			let andQuery = [{'configName' : configName}];
			let query = {$and:andQuery};
			
			
			let cursor = db.collection('position').find(query);
			let arr = [];
			cursor.forEach(function(doc) {
				
				if(doc.classifiedAs == 0)
				{
					if(doc.percent <= minimalPercentBlacken)
					{
						doc.classifiedAs = 1;
						if(doc.actualClass == 0)
						{
							doc.statisticClass ='false-negativ';
						}
						else
							doc.statisticClass = 'true-negativ';
					}
				}
				else
				{
					if(doc.percent <= minimalPercentGrass)
					{
						doc.classifiedAs = 0;
						if(doc.actualClass == 0)
						{
							doc.statisticClass = 'true-positiv';
						}
						else
						{
							doc.statisticClass = "false-positiv";
						}
						
					}
				}
				
				arr.push(doc);
			}, function(done) 
			{
				config.sort();
				res.json({config: config,content: arr});
			});
		
		});
		
	});
});

app.listen(8080)
