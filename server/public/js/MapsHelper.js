function MapsHelper()
{
	this.calcCenterPoint = function(points)
	{
		let center = {lat:0,lng:0};
		if(points.length >0)
		{
			points.forEach(function(ele)
			{
				center.lat += ele.lat;
				center.lng += ele.lng;
				
			});
			
			center.lat/= points.length;
			center.lng/=points.length;
		}
		return center;
	};
	this.calcDistance  = function(p1,p2)
	{
		let degreeToMeterRatio = 111300.0;//an approximation, for small distances ok
		
		let deltaLat = (p1.lat-p2.lat)*degreeToMeterRatio;
		let deltaLng = (p1.lng-p2.lng)*degreeToMeterRatio;
		
		return Math.sqrt(deltaLat*deltaLat+deltaLng*deltaLng);
	
		
		
	};
	this.calcArea = function(polygon)
	{
		degreeToMeterRatio = 111300.0;//an approximation, for small distances ok
		
		function Vector(x,y)
		{
			this.x = x;
			this.y = y;
		};
		let points = [];
		for(let i = 1; i < polygon.length;i++)
		{
			points.push(new Vector(polygon[i][0]*degreeToMeterRatio-polygon[0][0]*degreeToMeterRatio,
				polygon[i][1]*degreeToMeterRatio-polygon[0][1]*degreeToMeterRatio));		
		}
		points.push(new Vector(0,0));

		let d11 = new Vector(points[0].x - points[1].x,points[0].y - points[1].y);
		let d21 = new Vector(points[0].x - points[3].x,points[0].y - points[3].y);
		let l11 = Math.sqrt(d11.x*d11.x+d11.y*d11.y);
		let l21 = Math.sqrt(d21.x*d21.x+d21.y*d21.y);
		let angle1= (d11.x*d21.y+d11.y*d21.y)/(l11*l21);
		let area1 = 0.5*Math.sin(Math.acos(angle1))*l11*l21;
		
		let d12 = new Vector(points[1].x - points[2].x,points[1].y - points[2].y);
		let d22 = new Vector(points[1].x - points[3].x,points[1].y - points[3].y);
		let l12 = Math.sqrt(d12.x*d12.x+d12.y*d12.y);
		let l22 = Math.sqrt(d22.x*d22.x+d22.y*d22.y);
		let angle2 = (d12.x*d22.x+d12.y*d22.y)/(l12*l22);
		let area2 = 0.5*Math.sin(Math.acos(angle2))*l12*l22;	

		return area2+area1;
	};
};
