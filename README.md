# Setup instructions for ubuntu 16.04

1. install mongodb 3 by following this installation instruction:
    https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-mongodb-on-ubuntu-16-04
3. create database entries and images by following this instruction:
    https://gitlab.com/tscheims1/blacken-detection/tree/master/tester-and-visualizer
4. clone repository
    `git clone https://gitlab.com/tscheims1/blacken-displayer`
5. install node and node's package manager
    ```apt-get install nodejs npm```
6. install all node packages <br />
``` cd blacken-displayer/server
    npm install```

# start server

``` cd blacken-displayer/server
 nodejs server.js&```

# start server as a daemon

1. modify the serice file: https://gitlab.com/tscheims1/blacken-displayer/blob/master/blacken-displayer.service<br />
   (at least change the absolute paths in the options ExecStart and WorkingDirectory)
2. copy the blacken-displayer.service file in the directory `/etc/systemd/system`
3. register the service: `sudo systemctl enable blacken-displayer.service` 
4. start the service: `sudo systemctl start blacken-displayer.service` 

# access the interactive map
 
 1. run a webbrowser 
 2. open http://servername:8080